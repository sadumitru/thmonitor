from requests.exceptions import ConnectionError
import time
import adafruit_dht
import board
from datetime import datetime
import pytz
import os
import requests


def get_current_datetime_utc():
    local = pytz.timezone("Europe/Bucharest")
    local_dt = local.localize(datetime.now(), is_dst=None)
    utc_dt = local_dt.astimezone(pytz.utc)

    return local_dt.astimezone(pytz.utc).strftime("%Y-%m-%dT%H:%M")


def send_data(location, temp, hum):
    url = "http://192.168.1.112:8000/api/"

    payload = {'time': get_current_datetime_utc(),
               'sensor_location': location,
               'temperature': temp,
               'humidity': hum,
               'owner': '1'}
    files = []
    headers = {
        'Authorization': 'Token a37e5983a52e1d7bbde61827dec87e5cca5a4feb'
    }

    response = requests.request(
        "POST", url, headers=headers, data=payload, files=files)

    print(response.text)


SENSOR_LOCATION_NAME = "Sorina's Room"
MINUTES_BETWEEN_READS = 5

dhtSensor = adafruit_dht.DHT22(board.D4)

while True:
    try:
        humidity = dhtSensor.humidity
        temp_c = dhtSensor.temperature

        send_data(SENSOR_LOCATION_NAME, str(temp_c), str(humidity))
    except RuntimeError:
        print("RuntimeError, trying again...")
        continue
    except ConnectionError:
        print("Network issue, trying again...")
        continue
    finally:
        time.sleep(60*MINUTES_BETWEEN_READS)
