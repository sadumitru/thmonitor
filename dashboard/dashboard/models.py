from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse

class SensorLog(models.Model): 
    time = models.DateTimeField()
    sensor_location = models.CharField(max_length=100)
    temperature = models.FloatField()
    humidity = models.FloatField()
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.sensor_location
