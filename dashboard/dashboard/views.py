from django.shortcuts import render, get_object_or_404, redirect
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)
from .models import SensorLog
from django.http import JsonResponse
from datetime import datetime, timedelta, timezone
from django.conf import settings
import pytz
from .forms import ContactForm
from django.contrib import messages

# RestAPI framework
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import SensorLogSerializer


class ChartData(APIView):
    authentication_classes = []
    permission_classes = []


    def get(self, request, format=None):
        local = pytz.timezone(settings.TIME_ZONE)
        local_dt = local.localize(datetime.now() - timedelta(hours=6), is_dst=None)
        utc_dt = local_dt.astimezone(pytz.utc)

        rooms = SensorLog.objects.values("sensor_location").distinct().order_by('sensor_location')
        result = []

        for room in rooms:
            entries = SensorLog.objects.filter(sensor_location=room["sensor_location"], time__gte=utc_dt).order_by('time')
            labels = []
            temp_data = []
            humidity_data = []

            for entry in entries:
                labels.append(entry.time.astimezone(pytz.timezone(settings.TIME_ZONE)).strftime("%H:%M"))
                temp_data.append(entry.temperature)
                humidity_data.append(entry.humidity)

            result.append({
                "labels": labels,
                "tempData": temp_data,
                "humidityData": humidity_data
            })

        return Response(result)

class ChartData2(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, format=None):
        local = pytz.timezone(settings.TIME_ZONE)

        if 'start_date' in request.GET:
            start_date = datetime.strptime(request.GET['start_date'], '%Y-%m-%d')
            start_date = local.localize(start_date, is_dst=None)


        if 'end_date' in request.GET:
            end_date = datetime.strptime(request.GET['end_date'], '%Y-%m-%d')
            end_date = local.localize(end_date, is_dst=None)

        if not start_date or not end_date:
            error = True

        rooms = SensorLog.objects.values("sensor_location").distinct().order_by('sensor_location')
        result = []

        for room in rooms:
            entries = SensorLog.objects.filter(sensor_location=room["sensor_location"], time__range=[start_date, end_date])
            labels = []
            temp_data = []
            humidity_data = []

            for entry in entries:
                labels.append(entry.time.astimezone(pytz.timezone(settings.TIME_ZONE)).strftime("%H:%M"))
                temp_data.append(entry.temperature)
                humidity_data.append(entry.humidity)

            result.append({
                "labels": labels,
                "tempData": temp_data,
                "humidityData": humidity_data
            })

        return Response(result)


class ApiV1View(APIView):
    permission_classes = (IsAuthenticated, )


    def get(self, request, *args, **kwargs):
        qs = SensorLog.objects.all()
        serializer = SensorLogSerializer(qs, many=True)

        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        serializer = SensorLogSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors)


class RoomsListView(ListView):
    model = SensorLog
    template_name = 'dashboard/home.html'  # <app>/<model>_<viewtype>.html
    context_object_name = 'rooms'
    ordering = ['sensor_location']
    paginate_by = 5
    
    def get_queryset(self):
        return SensorLog.objects.values("sensor_location").distinct().order_by('sensor_location')


class RoomsHistoryListView(ListView):
    model = SensorLog
    template_name = 'dashboard/history.html'  # <app>/<model>_<viewtype>.html
    context_object_name = 'rooms'
    ordering = ['sensor_location']
    paginate_by = 5
    
    def get_queryset(self):
        return SensorLog.objects.values("sensor_location").distinct().order_by('sensor_location')

def about(request):
    return render(request, 'dashboard/about.html', {'title': 'About'})


def contactView(request):
    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            email = form.cleaned_data['email']
            message = form.cleaned_data['message']
            try:
                send_mail(subject, message, email, [settings.EMAIL_HOST_USER, email])
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            
            messages.success(request, f'Email sent successfully!')
            return redirect('dashboard-contact')
    return render(request, "dashboard/contact.html", {'form': form})
