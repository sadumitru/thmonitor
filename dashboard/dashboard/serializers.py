from rest_framework import serializers
from .models import SensorLog

class SensorLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = SensorLog
        fields = (
            'time','sensor_location','temperature','humidity','owner'
        )
