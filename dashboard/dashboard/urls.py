from django.urls import path
from .views import (
    RoomsListView,
    RoomsHistoryListView
)
from . import views
from .views import contactView


urlpatterns = [
    path('', RoomsListView.as_view(), name='dashboard-home'),
    path('about/', views.about, name='dashboard-about'),
    path('history/', RoomsHistoryListView.as_view(), name='dashboard-history'),
    path('contact/', contactView, name='dashboard-contact'),
]
