#!/bin/bash

function upload() {
    TIME="$1"
    SENSOR_LOCATION="$2"
    TEMP="$3"
    HUMIDITY="$4"

    curl --location --request POST 'http://127.0.0.1:8000/api/' \
        --header 'Authorization: Token a37e5983a52e1d7bbde61827dec87e5cca5a4feb' \
        --form "time=\"${TIME}\"" \
        --form "sensor_location=\"${SENSOR_LOCATION}\"" \
        --form "temperature=\"${TEMP}\"" \
        --form "humidity=\"${HUMIDITY}\"" \
        --form 'owner="1"'
    echo
}

function get_random_float() {
    A="$1"
    B="$2"

    printf '%s\n' $(echo "scale=2; $RANDOM/32768*($B-$A) + $A" | bc )
}

START_DATETIME="2021-01-10T18:25"
END_DATETIME="2021-01-11T01:10"
TEMP_RANGE=(21 22)
HUMIDITY_RANGE=(53 58)

if ! date -d $START_DATETIME &> /dev/null || ! date -d $END_DATETIME &> /dev/null; then 
    echo "Invalid START_DATETIME ($START_DATETIME) or END_DATETIME ($END_DATETIME)!"; 
    exit 1; 
fi

if [[ "$(date -d $START_DATETIME +%s)" -gt "$(date -d $END_DATETIME +%s)" ]]; then 
    echo "START_DATETIME ($START_DATETIME) should be before END_DATETIME ($END_DATETIME)."; 
    exit 1; 
fi

I=0; 
while true; do 
    DATE=$(date -d "$START_DATETIME $I minute" +%Y-%m-%dT%H:%M)
    
    for SENSOR_LOCATION in "Livingroom"; do
            TEMP=$(get_random_float "${TEMP_RANGE[0]}" "${TEMP_RANGE[1]}")
            HUM=$(get_random_float "${HUMIDITY_RANGE[0]}" "${HUMIDITY_RANGE[1]}")

            upload "$DATE" "$SENSOR_LOCATION" "$TEMP" "$HUM"
    done

    if [[ "$DATE" == "$END_DATETIME" ]]; then 
        break; 
    fi; 

    I=$(($I+5)); 
done
